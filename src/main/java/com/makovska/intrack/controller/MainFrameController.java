package com.makovska.intrack.controller;

import com.makovska.intrack.entity.ComputerUsage;
import com.makovska.intrack.repository.impl.ComputerUsageRepository;
import com.makovska.intrack.utils.AlertUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

@Controller
@RequiredArgsConstructor
public class MainFrameController implements Initializable {

    private final ComputerUsageRepository usageRepository;
    @FXML
    public TableColumn<ComputerUsage, String> username;
    @FXML
    public TableColumn<ComputerUsage, String> computerId;
    @FXML
    public TableColumn<ComputerUsage, String> sTime;
    @FXML
    public TableColumn<ComputerUsage, String> eTime;
    @FXML
    private TextField usernameField;
    @FXML
    private TextField computerField;
    @FXML
    private TextField sTimeField;
    @FXML
    private TextField eTimeField;
    @FXML
    private TableView<ComputerUsage> tbData;
    private ObservableList<ComputerUsage> usageInfos = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        username.setCellValueFactory(new PropertyValueFactory<>("username"));
        computerId.setCellValueFactory(new PropertyValueFactory<>("computerId"));
        sTime.setCellValueFactory(new PropertyValueFactory<>("sTime"));
        eTime.setCellValueFactory(new PropertyValueFactory<>("eTime"));
        loadDetails();
    }


    private void loadDetails() {
        usageInfos.clear();
        usageInfos.addAll(usageRepository.findAll().toArray(new ComputerUsage[0]));

        tbData.setItems(usageInfos);
    }

    public void saveNew() {
        LocalDateTime sDateParsed;
        LocalDateTime eDateParsed;
        try {
            sDateParsed = LocalDateTime.parse(sTimeField.getText());
            eDateParsed = LocalDateTime.parse(eTimeField.getText());
        } catch (Exception e) {
            AlertUtils.showAlert(Alert.AlertType.ERROR, null, "Invalid date format!",
                    "Use ISO_LOCAL_DATE_TIME format:\nYYYY-MM-ddTHH:mm:ss\nf.g.: 2019-12-30T23:30:00");
            return;
        }
        ComputerUsage computerUsage = new ComputerUsage(null, usernameField.getText(), computerField.getText(), sDateParsed, eDateParsed);
        usageRepository.save(computerUsage);
        loadDetails();
        clearFields();
    }

    private void clearFields() {
        usernameField.setText(null);
        computerField.setText(null);
        sTime.setText(null);
        eTime.setText(null);
    }

    public void deleteRow() {
        List<ComputerUsage> usages = tbData.getSelectionModel().getSelectedItems();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to delete selected?");
        Optional<ButtonType> action = alert.showAndWait();

        if (action.isPresent() && action.get() == ButtonType.OK) {
            usages.forEach(it -> usageRepository.delete(it.getUsageId()));
        }

        loadDetails();
    }
}
