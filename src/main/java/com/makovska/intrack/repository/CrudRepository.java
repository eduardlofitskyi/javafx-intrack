package com.makovska.intrack.repository;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T, ID> {
    T save(T object);

    List<T> saveAll(List<T> objects);

    boolean delete(ID objectId);

    List<T> findAll();

    Optional<T> findById(ID objectId);

    T update(T object);
}
