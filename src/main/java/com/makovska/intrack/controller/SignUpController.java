package com.makovska.intrack.controller;

import com.makovska.intrack.entity.User;
import com.makovska.intrack.repository.impl.UserRepository;
import com.makovska.intrack.utils.AlertUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Window;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import view.StageManager;

import java.util.Optional;

@Controller
@RequiredArgsConstructor
@Lazy
@Slf4j
public class SignUpController {

    private final StageManager stageManager;
    private final UserRepository userRepository;

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private PasswordField passwordConfirmField;

    @FXML
    private Button submitButton;

    @FXML
    protected void handleSubmitButtonAction() {
        Window owner = submitButton.getScene().getWindow();
        if (validateEmptyFields(owner)) return;

        if (!passwordConfirmField.getText().equals(passwordField.getText())) {
            AlertUtils.showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Passwords don't match");
            return;
        }

        Optional<User> user = userRepository.findByUsername(usernameField.getText());


        if (!user.isPresent()) {
            User userToCreate = User.builder()
                    .password(passwordField.getText())
                    .username(usernameField.getText())
                    .build();
            User createdUser = userRepository.save(userToCreate);
            log.info("New user created: user={}", createdUser);

            stageManager.switchScene("/fxml/signin.fxml", "Sign In");
            AlertUtils.showAlert(Alert.AlertType.INFORMATION, owner, "Registration Successful!",
                    "User Created " + usernameField.getText());
        } else {
            AlertUtils.showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "User with such name already exist");
        }

    }

    private boolean validateEmptyFields(Window owner) {
        if (usernameField.getText().isEmpty()) {
            AlertUtils.showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter your uniq name");
            return true;
        }
        if (passwordField.getText().isEmpty()) {
            AlertUtils.showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter a password");
            return true;
        }
        if (passwordConfirmField.getText().isEmpty()) {
            AlertUtils.showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter a password confirmation");
            return true;
        }
        return false;
    }

    @FXML
    public void handleSignInButtonAction() {
        stageManager.switchScene("/fxml/signin.fxml", "Sign In");
    }
}

