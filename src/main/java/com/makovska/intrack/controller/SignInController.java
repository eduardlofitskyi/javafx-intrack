package com.makovska.intrack.controller;

import com.makovska.intrack.entity.User;
import com.makovska.intrack.repository.impl.UserRepository;
import com.makovska.intrack.state.SessionState;
import com.makovska.intrack.utils.AlertUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Window;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import view.StageManager;

import java.util.Optional;

@Controller
@RequiredArgsConstructor
@Lazy
public class SignInController {

    private final StageManager stageManager;
    private final UserRepository userRepository;
    private final SessionState sessionState;
    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField computerNumField;

    @FXML
    private Button submitButton;

    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) {
        Window owner = submitButton.getScene().getWindow();
        if (validateEmptyFields(owner)) return;

        Optional<User> user = userRepository.findByUsername(usernameField.getText());

        if (user.isPresent() && user.get().getPassword().equals(passwordField.getText())) {
            sessionState.setCurrentUser(usernameField.getText());
            sessionState.setCurrentComputer(computerNumField.getText());
            stageManager.switchScene("/fxml/main-table.fxml", "InTrack Application");
        } else {
            AlertUtils.showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Invalid principals");
        }

    }

    private boolean validateEmptyFields(Window owner) {
        if (usernameField.getText().isEmpty()) {
            AlertUtils.showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter your name");
            return true;
        }
        if (passwordField.getText().isEmpty()) {
            AlertUtils.showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter a password");
            return true;
        }
        if (computerNumField.getText().isEmpty()) {
            AlertUtils.showAlert(Alert.AlertType.ERROR, owner, "Form Error!",
                    "Please enter number of computer");
            return true;
        }
        return false;
    }

    @FXML
    public void handleSignUpButtonAction() {
        stageManager.switchScene("/fxml/signup.fxml", "Sign Up");
    }
}

