package com.makovska.intrack.service;

import com.makovska.intrack.entity.User;

import java.util.List;

public interface StorageService {
    List<User> getAllUsers();
}
