package com.makovska.intrack.service;

public interface UserService {
    boolean isUserCredsCorrect(String username, String password);
}
