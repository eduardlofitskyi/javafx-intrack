package com.makovska.intrack.controller;

import com.makovska.intrack.entity.ComputerUsage;
import com.makovska.intrack.repository.impl.ComputerUsageRepository;
import com.makovska.intrack.utils.AggregationUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Controller
@RequiredArgsConstructor
public class DayTableController implements Initializable {

    private final ComputerUsageRepository usageRepository;
    @FXML
    public TableColumn<AggregationUtils.ComputerUsageByHours, String> username;
    @FXML
    public TableColumn<AggregationUtils.ComputerUsageByHours, String> computerId;
    @FXML
    public TableColumn<AggregationUtils.ComputerUsageByHours, Integer> timeSpend;
    @FXML
    private TableView<AggregationUtils.ComputerUsageByHours> tbDataDaily;
    private ObservableList<AggregationUtils.ComputerUsageByHours> usageInfos = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        username.setCellValueFactory(new PropertyValueFactory<>("username"));
        computerId.setCellValueFactory(new PropertyValueFactory<>("computerId"));
        timeSpend.setCellValueFactory(new PropertyValueFactory<>("timeSpend"));
        loadDetails();
    }


    private void loadDetails() {
        usageInfos.clear();
        List<ComputerUsage> all = usageRepository.findAll();

        List<AggregationUtils.ComputerUsageByHours> usageByHours = AggregationUtils.aggregateDaily(all);


        usageInfos.addAll(usageByHours.toArray(new AggregationUtils.ComputerUsageByHours[0]));

        tbDataDaily.setItems(usageInfos);
    }
}
