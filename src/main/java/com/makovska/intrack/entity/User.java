package com.makovska.intrack.entity;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @CsvBindByPosition(position = 0)
    private Long id;

    @NonNull
    @CsvBindByPosition(position = 1)
    private String username;

    @NonNull
    @EqualsAndHashCode.Exclude
    @CsvBindByPosition(position = 2)
    private String password;

}
