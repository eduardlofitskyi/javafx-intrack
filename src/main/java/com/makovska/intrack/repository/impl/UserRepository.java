package com.makovska.intrack.repository.impl;

import com.makovska.intrack.entity.User;
import com.makovska.intrack.repository.CrudRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.makovska.intrack.constants.FileConstants.COL_SEPARATOR;

@Service
@Slf4j
public class UserRepository implements CrudRepository<User, Long> {

    @Value("${storage.user.path}")
    private String filePath;

    @Override
    public User save(User user) {
        List<User> existUsers = readAll();
        if (existUsers.stream()
                .anyMatch(it -> Objects.equals(it.getUsername(), user.getUsername()))) {
            log.error("User with such username already exist: username={}", user.getUsername());
            return findByUsername(user.getUsername()).get();
        }

        user.setId(RandomUtils.nextLong());
        existUsers.add(user);
        writeAll(existUsers);
        return user;
    }

    @Override
    public List<User> saveAll(List<User> users) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Long userId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<User> findAll() {
        return readAll();
    }

    @Override
    public Optional<User> findById(Long userId) {
        return readAll().stream()
                .filter(it -> Objects.equals(it.getId(), userId))
                .findFirst();
    }

    public Optional<User> findByUsername(String username) {
        return readAll().stream()
                .filter(it -> Objects.equals(it.getUsername(), username))
                .findFirst();
    }

    @Override
    public User update(User user) {
        throw new UnsupportedOperationException();
    }

    private List<User> readAll() {
        try {
            if (createFileIfNotExist()) return Collections.emptyList();

            return FileUtils.readLines(new File(this.filePath), Charset.defaultCharset()).stream()
                    .map(it -> it.split(COL_SEPARATOR))
                    .filter(it -> it.length == 3)
                    .map(it -> new User(Long.parseLong(it[0]), it[1], it[2]))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    private void writeAll(List<User> users) {
        try {
            createFileIfNotExist();
            String dataToWrite = users.stream()
                    .map(it -> it.getId() + COL_SEPARATOR + it.getUsername() + COL_SEPARATOR + it.getPassword() + "\n")
                    .collect(Collectors.joining());

            FileUtils.write(new File(this.filePath), dataToWrite, Charset.defaultCharset(), false);
        } catch (IOException e) {
            log.error("Cannot write data to file");
        }
    }

    private boolean createFileIfNotExist() throws IOException {
        if (!Files.exists(Paths.get(this.filePath))) {
            Files.createFile(Paths.get(this.filePath));
            return true;
        }
        return false;
    }
}
