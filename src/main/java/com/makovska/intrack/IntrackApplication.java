package com.makovska.intrack;

import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import view.StageManager;

@SpringBootApplication
public class IntrackApplication extends Application {

    private ConfigurableApplicationContext springContext;
    private StageManager stageManager = null;

    public static void main(final String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() {
        springContext = bootstrapSpringApplicationContext();
    }

    @Override
    public void start(Stage stage) {
        stageManager = springContext.getBean(StageManager.class, stage);
        displayInitialScene();
    }

    @Override
    public void stop() {
        springContext.close();
    }

    protected void displayInitialScene() {
        stageManager.switchScene("/fxml/signin.fxml", "Sign In");
    }

    private ConfigurableApplicationContext bootstrapSpringApplicationContext() {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(IntrackApplication.class);
        String[] args = getParameters().getRaw().stream().toArray(String[]::new);
        builder.headless(false);
        return builder.run(args);
    }

}
