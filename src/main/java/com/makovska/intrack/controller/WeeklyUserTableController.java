package com.makovska.intrack.controller;

import com.makovska.intrack.entity.ComputerUsage;
import com.makovska.intrack.repository.impl.ComputerUsageRepository;
import com.makovska.intrack.utils.AggregationUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Controller
@RequiredArgsConstructor
public class WeeklyUserTableController implements Initializable {
    private final ComputerUsageRepository usageRepository;
    @FXML
    public TableColumn<AggregationUtils.WeeklyUserUsage, String> username;
    @FXML
    public TableColumn<AggregationUtils.WeeklyUserUsage, String> timeSpend;
    @FXML
    private TableView<AggregationUtils.WeeklyUserUsage> tbDataWeeklyUser;
    private ObservableList<AggregationUtils.WeeklyUserUsage> usageInfos = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        username.setCellValueFactory(new PropertyValueFactory<>("username"));
        timeSpend.setCellValueFactory(new PropertyValueFactory<>("timeSpend"));
        loadDetails();
    }

    private void loadDetails() {
        usageInfos.clear();
        List<ComputerUsage> allUsages = usageRepository.findAll();

        List<AggregationUtils.WeeklyUserUsage> usageByHours = AggregationUtils.aggregateWeeklyByUser(allUsages);

        usageInfos.addAll(usageByHours.toArray(new AggregationUtils.WeeklyUserUsage[0]));

        tbDataWeeklyUser.setItems(usageInfos);
    }

}
