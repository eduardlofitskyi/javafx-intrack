package com.makovska.intrack.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class ComputerUsage {
    private Long usageId;
    private String username;
    private String computerId;
    private LocalDateTime sTime;
    private LocalDateTime eTime;
}
