package com.makovska.intrack.controller;

import com.makovska.intrack.constants.FileConstants;
import com.makovska.intrack.entity.ComputerUsage;
import com.makovska.intrack.repository.impl.ComputerUsageRepository;
import com.makovska.intrack.state.SessionState;
import com.makovska.intrack.utils.AggregationUtils;
import com.makovska.intrack.utils.AlertUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import view.StageManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@Lazy
@Slf4j
public class MenuController implements Initializable {

    private final StageManager stageManager;
    private final ComputerUsageRepository computerUsageRepository;
    private final SessionState sessionState;

    @FXML
    private MenuItem startTracking;
    @FXML
    private MenuItem stopTracking;

    public void logout(ActionEvent actionEvent) {
        sessionState.setCurrentUser(null);
        sessionState.setCurrentComputer(null);
        stageManager.switchScene("/fxml/signin.fxml", "Sign In");
    }

    public void startTrack() {
        startTracking.setDisable(true);
        stopTracking.setDisable(false);
        sessionState.setSTime(LocalDateTime.now());
    }

    public void stopTrack() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime sTime = sessionState.getSTime();

        computerUsageRepository.save(new ComputerUsage(null, sessionState.getCurrentUser(), sessionState.getCurrentComputer(), sTime, now));

        sessionState.setSTime(null);
        startTracking.setDisable(false);
        stopTracking.setDisable(true);

    }

    public void mainFrame() {
        stageManager.switchScene("/fxml/main-table.fxml", "InTrack Application");
    }

    public void developedBy(ActionEvent actionEvent) {
        AlertUtils.showAlert(Alert.AlertType.INFORMATION, null, "Developed By",
                "2nd year student\n" +
                        "groups 6.04.122.010.17.01\n" +
                        "specialty 122\n" +
                        "\"Computer Science\"\n" +
                        "first (bachelor level)\n" +
                        "Angelika Makovska");
    }

    public void about(ActionEvent actionEvent) {
        AlertUtils.showAlert(Alert.AlertType.INFORMATION, null, "About",
                "Computer Timer is a free personal computer monitoring program.\nThe timer" +
                        " keeps track of activity in all programs\nThis " +
                        "software product will be useful to anyone who works on a computer");
    }

    public void export() throws IOException {
        List<ComputerUsage> all = computerUsageRepository.findAll();

        ButtonType buttonTypeDaily = new ButtonType("Daily");
        ButtonType buttonTypeWeekly = new ButtonType("Weekly");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert timelineAlert = new Alert(Alert.AlertType.CONFIRMATION, "Choose timeline", buttonTypeDaily, buttonTypeWeekly, buttonTypeCancel);
        timelineAlert.setHeaderText("Dashboard Time");
        timelineAlert.setTitle("Dashboard");
        Optional<ButtonType> timelineResult = timelineAlert.showAndWait();

        if (timelineResult.get() == buttonTypeDaily) {
            exportDataToFile(AggregationUtils.aggregateDaily(all));
        } else if (timelineResult.get() == buttonTypeWeekly) {
            ButtonType buttonTypeComputer = new ButtonType("Computers");
            ButtonType buttonTypeUser = new ButtonType("Users");
            Alert dataTypeAlert = new Alert(Alert.AlertType.CONFIRMATION, "Choose type of dashboard", buttonTypeComputer, buttonTypeUser, buttonTypeCancel);
            dataTypeAlert.setHeaderText("Dashboard Type");
            dataTypeAlert.setTitle("Dashboard");
            Optional<ButtonType> typeResult = dataTypeAlert.showAndWait();

            if (typeResult.get() == buttonTypeComputer) {
                exportDataToFile(AggregationUtils.aggregateWeeklyByComputer(all));
            } else if (typeResult.get() == buttonTypeUser) {
                exportDataToFile(AggregationUtils.aggregateWeeklyByUser(all));
            }
        }
    }

    private void exportDataToFile(List<? extends Object> dataToSave) throws IOException {
        String fileName = "export_" + LocalDateTime.now() + ".csv";

        AlertUtils.showAlert(Alert.AlertType.CONFIRMATION, null, "Export",
                "Data will be exported to " + Paths.get(fileName).toAbsolutePath().toString());

        String data = dataToSave.stream()
                .map(Object::toString)
                .collect(Collectors.joining());
        FileUtils.write(new File(fileName),  data, Charset.defaultCharset());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (sessionState.getSTime() != null) {
            startTracking.setDisable(true);
            stopTracking.setDisable(false);
        } else {
            startTracking.setDisable(false);
            stopTracking.setDisable(true);
        }
    }

    public void dashboard(ActionEvent actionEvent) {
        ButtonType buttonTypeDaily = new ButtonType("Daily");
        ButtonType buttonTypeWeekly = new ButtonType("Weekly");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert timelineAlert = new Alert(Alert.AlertType.CONFIRMATION, "Choose timeline", buttonTypeDaily, buttonTypeWeekly, buttonTypeCancel);
        timelineAlert.setHeaderText("Dashboard Time");
        timelineAlert.setTitle("Dashboard");
        Optional<ButtonType> timelineResult = timelineAlert.showAndWait();

        if (timelineResult.get() == buttonTypeDaily) {
            stageManager.switchScene("/fxml/day-table.fxml", "Daily Chart");
        } else if (timelineResult.get() == buttonTypeWeekly) {
            ButtonType buttonTypeComputer = new ButtonType("Computers");
            ButtonType buttonTypeUser = new ButtonType("Users");
            Alert dataTypeAlert = new Alert(Alert.AlertType.CONFIRMATION, "Choose type of dashboard", buttonTypeComputer, buttonTypeUser, buttonTypeCancel);
            dataTypeAlert.setHeaderText("Dashboard Type");
            dataTypeAlert.setTitle("Dashboard");
            Optional<ButtonType> typeResult = dataTypeAlert.showAndWait();

            if (typeResult.get() == buttonTypeComputer) {
                stageManager.switchScene("/fxml/weekly-comp-table.fxml", "Weekly Computer Usage");
            } else if (typeResult.get() == buttonTypeUser) {
                stageManager.switchScene("/fxml/weekly-user-table.fxml", "Weekly User Usage");
            }
        }
    }
}

