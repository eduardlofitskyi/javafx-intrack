package com.makovska.intrack.repository.impl;

import com.makovska.intrack.entity.ComputerUsage;
import com.makovska.intrack.repository.CrudRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.makovska.intrack.constants.FileConstants.COL_SEPARATOR;

@Service
@Slf4j
public class ComputerUsageRepository implements CrudRepository<ComputerUsage, Long> {

    @Value("${storage.computerUsage.path}")
    private String filePath;

    @Override
    public ComputerUsage save(ComputerUsage usage) {
        List<ComputerUsage> existUsage = readAll();
        usage.setUsageId(RandomUtils.nextLong());
        existUsage.add(usage);
        writeAll(existUsage);
        return usage;
    }

    @Override
    public List<ComputerUsage> saveAll(List<ComputerUsage> usages) {
        return null;
    }

    @Override
    public boolean delete(Long usageId) {
        List<ComputerUsage> actual = readAll();
        List<ComputerUsage> filtered = actual.stream()
                .filter(it -> !Objects.equals(it.getUsageId(), usageId))
                .collect(Collectors.toList());

        writeAll(filtered);
        return filtered.size() != actual.size();
    }

    @Override
    public List<ComputerUsage> findAll() {
        return readAll();
    }

    @Override
    public Optional<ComputerUsage> findById(Long usageId) {
        return readAll().stream()
                .filter(it -> Objects.equals(it.getUsageId(), usageId))
                .findFirst();
    }

    @Override
    public ComputerUsage update(ComputerUsage usage) {
        return null;
    }

    private List<ComputerUsage> readAll() {
        try {
            if (createFileIfNotExist()) return Collections.emptyList();

            return FileUtils.readLines(new File(this.filePath), Charset.defaultCharset()).stream()
                    .map(it -> it.split(COL_SEPARATOR))
                    .filter(it -> it.length == 5)
                    .map(it -> new ComputerUsage(Long.parseLong(it[0]), it[1], it[2], LocalDateTime.parse(it[3]), LocalDateTime.parse(it[4])))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    private void writeAll(List<ComputerUsage> users) {
        try {
            createFileIfNotExist();
            String dataToWrite = users.stream()
                    .map(it -> it.getUsageId() + COL_SEPARATOR + it.getUsername() + COL_SEPARATOR + it.getComputerId() + COL_SEPARATOR + it.getSTime() + COL_SEPARATOR + it.getETime() + "\n")
                    .collect(Collectors.joining());

            FileUtils.write(new File(this.filePath), dataToWrite, Charset.defaultCharset(), false);
        } catch (IOException e) {
            log.error("Cannot write data to file");
        }
    }

    private boolean createFileIfNotExist() throws IOException {
        if (!Files.exists(Paths.get(this.filePath))) {
            Files.createFile(Paths.get(this.filePath));
            return true;
        }
        return false;
    }
}
