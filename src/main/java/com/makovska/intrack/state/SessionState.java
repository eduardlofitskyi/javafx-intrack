package com.makovska.intrack.state;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Data
public class SessionState {
    private String currentUser;
    private String currentComputer;
    private LocalDateTime sTime;
}
