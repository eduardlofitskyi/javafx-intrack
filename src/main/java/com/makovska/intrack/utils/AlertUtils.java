package com.makovska.intrack.utils;

import javafx.scene.control.Alert;
import javafx.stage.Window;
import lombok.experimental.UtilityClass;

@UtilityClass
public class AlertUtils {

    public static void showAlert(
            Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
}
