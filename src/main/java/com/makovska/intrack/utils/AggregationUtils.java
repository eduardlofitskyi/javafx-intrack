package com.makovska.intrack.utils;

import com.makovska.intrack.constants.FileConstants;
import com.makovska.intrack.entity.ComputerUsage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.UtilityClass;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.makovska.intrack.constants.FileConstants.COL_SEPARATOR;

@UtilityClass
public class AggregationUtils {

    public static List<WeeklyComputerUsage> aggregateWeeklyByComputer(List<ComputerUsage> all) {
        all = all.stream().filter(it -> it.getETime().compareTo(LocalDateTime.now().minusDays(7)) > 0).collect(Collectors.toList());

        Map<String, List<ComputerUsage>> collect = all.stream()
                .collect(Collectors.groupingBy(ComputerUsage::getComputerId));


        List<AggregationUtils.WeeklyComputerUsage> usageByHours = new LinkedList<>();
        for (Map.Entry<String, List<ComputerUsage>> compUsage : collect.entrySet()) {
            String computerId = compUsage.getKey();
            long timeSpend = compUsage.getValue().stream().mapToLong(it -> Duration.between(it.getSTime(), it.getETime()).toMinutes()).sum();
            usageByHours.add(new AggregationUtils.WeeklyComputerUsage(computerId, Long.toString(timeSpend)));
        }
        return usageByHours;
    }

    public static List<WeeklyUserUsage> aggregateWeeklyByUser(List<ComputerUsage> all) {
        all = all.stream().filter(it -> it.getETime().compareTo(LocalDateTime.now().minusDays(7)) > 0).collect(Collectors.toList());

        Map<String, List<ComputerUsage>> collect = all.stream()
                .collect(Collectors.groupingBy(ComputerUsage::getUsername));


        List<WeeklyUserUsage> usageByHours = new LinkedList<>();
        for (Map.Entry<String, List<ComputerUsage>> compUsage : collect.entrySet()) {
            String username = compUsage.getKey();
            long timeSpend = compUsage.getValue().stream().mapToLong(it -> Duration.between(it.getSTime(), it.getETime()).toMinutes()).sum();
            usageByHours.add(new WeeklyUserUsage(username, Long.toString(timeSpend)));
        }
        return usageByHours;
    }

    public static List<ComputerUsageByHours> aggregateDaily(List<ComputerUsage> all) {
        all = all.stream().filter(it -> it.getETime().compareTo(LocalDateTime.now().minusDays(1)) > 0).collect(Collectors.toList());

        Map<String, List<ComputerUsage>> collect = all.stream()
                .collect(Collectors.groupingBy(ComputerUsage::getUsername));

        List<ComputerUsageByHours> usageByHours = new LinkedList<>();
        for (Map.Entry<String, List<ComputerUsage>> personUsage : collect.entrySet()) {
            HashMap<String, Long> objectObjectHashMap = new HashMap<>();
            for (ComputerUsage usage : personUsage.getValue()) {
                if (objectObjectHashMap.containsKey(usage.getComputerId())) {
                    objectObjectHashMap.put(usage.getComputerId(), objectObjectHashMap.get(usage.getComputerId()) + Duration.between(usage.getSTime(), usage.getETime()).toMinutes());
                } else {
                    objectObjectHashMap.put(usage.getComputerId(), Duration.between(usage.getSTime(), usage.getETime()).toMinutes());
                }
            }
            for (Map.Entry<String, Long> hours : objectObjectHashMap.entrySet()) {
                usageByHours.add(new ComputerUsageByHours(personUsage.getKey(), hours.getKey(), hours.getValue()));
            }
        }
        return usageByHours;
    }

    @Data
    @AllArgsConstructor
    public class WeeklyComputerUsage {
        private String computerId;
        private String timeSpend;

        @Override
        public String toString() {
            return computerId + COL_SEPARATOR + timeSpend + "\n";
        }
    }

    @Data
    @AllArgsConstructor
    public class WeeklyUserUsage {
        private String username;
        private String timeSpend;

        @Override
        public String toString() {
            return username + COL_SEPARATOR + timeSpend + "\n";
        }
    }

    @Data
    @AllArgsConstructor
    public class ComputerUsageByHours {
        private String username;
        private String computerId;
        private Long timeSpend;

        @Override
        public String toString() {
            return username + COL_SEPARATOR + computerId + COL_SEPARATOR + timeSpend + "\n";
        }
    }
}